
//This sketch is for controlling Plant Watering Machine by Raspberry Pi Pico...
//#include <elapsedMillis.h>

class elapsedMillis
{
private:
	unsigned long ms;
public:
	elapsedMillis(void) { ms = millis(); }
	elapsedMillis(unsigned long val) { ms = millis() - val; }
	elapsedMillis(const elapsedMillis &orig) { ms = orig.ms; }
	operator unsigned long () const { return millis() - ms; }
	elapsedMillis & operator = (const elapsedMillis &rhs) { ms = rhs.ms; return *this; }
	elapsedMillis & operator = (unsigned long val) { ms = millis() - val; return *this; }
	elapsedMillis & operator -= (unsigned long val)      { ms += val ; return *this; }
	elapsedMillis & operator += (unsigned long val)      { ms -= val ; return *this; }
	elapsedMillis operator - (int val) const           { elapsedMillis r(*this); r.ms += val; return r; }
	elapsedMillis operator - (unsigned int val) const  { elapsedMillis r(*this); r.ms += val; return r; }
	elapsedMillis operator - (long val) const          { elapsedMillis r(*this); r.ms += val; return r; }
	elapsedMillis operator - (unsigned long val) const { elapsedMillis r(*this); r.ms += val; return r; }
	elapsedMillis operator + (int val) const           { elapsedMillis r(*this); r.ms -= val; return r; }
	elapsedMillis operator + (unsigned int val) const  { elapsedMillis r(*this); r.ms -= val; return r; }
	elapsedMillis operator + (long val) const          { elapsedMillis r(*this); r.ms -= val; return r; }
	elapsedMillis operator + (unsigned long val) const { elapsedMillis r(*this); r.ms -= val; return r; }
};

const int pumpPos = 14;     /*connected to H-Bridges driver 2, when 
'HIGH' sets H-Bridges corresponding output 'on' ('plus')*/
const int pumpNeg = 15;     /*connected to H-Bridge's driver 1, when
'LOW' sets H-Bridges corresponding output 'off' ('minus'), therefore 
sets rotational direction*/
const int pumpSpeed = 16;  /*connected to H-Bridge's pin 
which enables/disables drivers 1 and 2 in H-Bridge*/
int sensorAnalogpin = 28;  //This takes input from moisturesensor
int moistureValue; // 'analog' value (0-539) for moisture detected by sensor
float moisture_percentage; //moistureValue converted to percentage of maximum
elapsedMillis modeTimer = 0;
elapsedMillis hourTimer = 0;

void setup() {
  Serial.begin(9600);
  pinMode(sensorAnalogpin, INPUT);
  pinMode(pumpPos, OUTPUT);
  pinMode(pumpNeg, OUTPUT);
  pinMode(pumpSpeed, OUTPUT);
  delay(1000);
}

long previousMillis = 0;

void loop() {
  
  const long noticeTime = 5000;
  
  if (modeTimer >= 1800000){ //sets mode to 'real' aka operational	
    digitalWrite(LED_BUILTIN, LOW);
    if(millis() - previousMillis >= noticeTime){
      Serial.print("\nMode for real...");
      previousMillis = millis();}
    if(hourTimer >= 3600000) {//one hour interval for checking moisture
      readMoisture();
      delay(100);
      pumpControl();
      hourTimer = 0;}
  }
  if(modeTimer<1800000){//testmode lasts for (61 seconds) half an hour
    digitalWrite(LED_BUILTIN, HIGH);
    if (hourTimer >= 20000){
    	Serial.print("\nStill in test mode...");
    	readMoisture();
    	delay(100);
      pumpControlTest();
      //pumpControl();
      hourTimer = 0;}	
  }
}

float readMoisture() {
  moistureValue = analogRead(sensorAnalogpin); //Reads moisture detected by sensor
  moisture_percentage = ((moistureValue / 950.00) * 100);//Sensor's scale is 0-950 
  Serial.print("\nMoisture sensor value: ");
  Serial.print(moistureValue);
  Serial.print("\nMoisture Value: ");
  Serial.print(moisture_percentage);
  Serial.print("%");
  return moisture_percentage;
}

void pumpControl() {
  elapsedMillis pumpTimer = 0;
  int count = 0;
  while(moisture_percentage < 32.0) {//condition when activates pump
    if(pumpTimer < 7000){
      digitalWrite(pumpSpeed, HIGH);  //activates H-Bridge's drivers 1 and 2
   	  digitalWrite(pumpPos, HIGH);//sets this driver 'on', red line to motor in circuit
      digitalWrite(pumpNeg, LOW);} //sets this driver 'off', green line to motor
    if (pumpTimer >= 7000){//pumps for 7 seconds(ca. 2 dl of water)
      //delay(7000);//pumps for 7 seconds(ca. 2 dl of water)  
      digitalWrite(pumpPos, LOW);}//stops pumping water
    if(pumpTimer == 12000){
      count++;}
    if(pumpTimer >= 307000){//waits 5 minutes till water has infiltrated soil
      readMoisture();    //checks again moisture in soil
      pumpTimer = 0;}//sets 'clock' again
    if(count > 2){ //won't get stuck in while-loop 'forever' in case moisture reading fails
      break;}
  }
}

void pumpControlTest() {
  elapsedMillis pumpTestTimer = 0;
  while (moisture_percentage < 32.0) {//condition when activates pump
    if(pumpTestTimer < 3500){
      digitalWrite(pumpSpeed, HIGH); //activates H-Bridge's drivers 1 and 2
      digitalWrite(pumpPos, HIGH); //sets this driver 'on', red line to motor in circuit
      digitalWrite(pumpNeg, LOW);}  //sets this driver 'off', green line to motor
    if(pumpTestTimer >= 3500){//pumps for 3.5 seconds
      digitalWrite(pumpPos, LOW);}//stops pumping water
    if(pumpTestTimer >= 13500){//waits 10 seconds 
      readMoisture();
      pumpTestTimer = 0;}
    if(modeTimer>=1800000){
      break;}
  }
}






